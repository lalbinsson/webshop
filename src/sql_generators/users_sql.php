<?php
	function prepStmtRegisterUser() {
		return "INSERT INTO users (user_id, first_name, last_name, email, street, postal_code, city, password_hash)
						VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
	}

	function userSqlTypeString() {
		return 'ssssssss';
	}

	function prepStmtCountUsersWithEmail() {
		return "SELECT count(1) from users WHERE email = ?";
	}
?>
