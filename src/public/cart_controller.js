function init() {
    updateCart();
}

function addToCart(productId) {
    var formData = new FormData();
    formData.append('csrf_token', document.getElementsByTagName('meta')[0].getAttribute('content'));
    formData.append('productId', productId);

    var xhr = new XMLHttpRequest();


    xhr.onload = function() {
        console.log("Status: " + this.status);
        if (this.status === 200) {
            const response = JSON.parse(this.response);
            document.getElementById('cartCount').innerHTML = response.cartCount;
            console.log("Produkt tillagd i cart. Count: " + response.cartCount);
        } else if (this.status === 403) {
            console.log("403 forbidden");
        }
    }


    xhr.open('POST', "src/php/add_to_cart.php");
    xhr.send(formData);
}

function removeFromCart(productId, removeAll, newQuantity) {
    var formData = new FormData();
    formData.append('productId', productId);
    formData.append('csrf_token', document.getElementsByTagName('meta')[0].getAttribute('content'));

    if (!removeAll && newQuantity <= 0) {
        removeAll = true;
    }

    formData.append('removeAll', removeAll)
    formData.append('newQuantity', newQuantity)

    var xhr = new XMLHttpRequest();


    xhr.onload = function() {
        console.log("Status: " + this.status);
        if (this.status === 200) {
            const response = JSON.parse(this.response);
            document.getElementById('cartCount').innerHTML = response.cartCount;
            console.log("Produkt borttagen från cart");
            updateCart();
        } else if (this.status === 403) {
            console.log("403 forbidden");
        }
    }

    xhr.open('POST', '/src/php/remove_from_cart.php');
    xhr.send(formData);
}

function updateCart() {
    const xhr = new XMLHttpRequest();
    xhr.onload = function() {
        if (this.status === 200) {
            document.getElementById('container').innerHTML = xhr.responseText;
        }
    }

    xhr.open('GET', '/src/php/cart_product_list.php');
    xhr.send();
}

function resetCart() {
    const xhr = new XMLHttpRequest();
    var formData = new FormData();
    formData.append('csrf_token', document.getElementsByTagName('meta')[0].getAttribute('content'));
    xhr.onload = function() {
        if (this.status === 200) {
            document.getElementById('container').innerHTML = xhr.responseText;
        }
    }

    xhr.open('POST', '/src/php/reset_cart.php');
    xhr.send(formData);
}
