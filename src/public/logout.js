function logout() {
	const xhr = new XMLHttpRequest();
	xhr.onload = function() {
		if (this.status === 200) {
			window.location.href = "/";
			location.reload();
		} else {
			// error
		}
	}

	xhr.open("POST", "/src/php/logout-user.php");
	xhr.send();
}
