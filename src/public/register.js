function register() {
	const firstName = document.getElementById('firstName');
	const lastName = document.getElementById('lastName');
	const email = document.getElementById('email');
	const street = document.getElementById('street');
	const postalCode = document.getElementById('postalCode');
	const city = document.getElementById('city');
	const password = document.getElementById('password');
	const confirmPassword = document.getElementById('confirmPassword');


	const formData = new FormData();
	formData.append('q', 'registerUser');
	formData.append("firstName", firstName.value);
	formData.append("lastName", lastName.value);
	formData.append("email", email.value);
	formData.append("street", street.value);
	formData.append("postalCode", postalCode.value);
	formData.append("city", city.value);
	formData.append("password", password.value);
	formData.append("confirmPassword", confirmPassword.value);

	const xhr = new XMLHttpRequest();
	xhr.onload = function() {
		console.log("STATUS: " + this.status);
		if (this.status === 201) {
			console.log("STATUS 201!");
			// registration successful.
			window.location.href = "/";
		} else if (this.status === 400) {
			// passwords did not match, weak password etc.
		} else {
			// other error
		}
	}

	xhr.open("POST", "/src/php/register-user.php");
	xhr.send(formData);
}

function onPasswordInput(password) {
	checkPasswordFormat(password);
	checkPasswordMatch(document.getElementById('confirmPassword').value);
}

function checkPasswordFormat(password) {
	const xhr = new XMLHttpRequest();
	xhr.onload = function() {
		if (this.status === 200) {
			document.getElementById('passwordValidLabel').innerHTML = this.responseText;
		}
	}

	const passwordData = new FormData();
	passwordData.append('q', 'checkPasswordFormat');
	passwordData.append('password', password);

	xhr.open('POST', "/src/php/register-user.php")
	xhr.send(passwordData);
}

function checkPasswordMatch(confirmPassword) {
	const password = document.getElementById('password').value;
	const passwordMatchLabel = document.getElementById('passwordMatchLabel');
	if (confirmPassword === password) {
		passwordMatchLabel.innerHTML = 'Passwords matches.'
	} else {
		passwordMatchLabel.innerHTML = 'Passwords does not match.'
	}
}
