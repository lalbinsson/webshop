function login() {
    const email = document.getElementById("email");
    const password = document.getElementById("password");

    const formData = new FormData();
    formData.append("email", email.value);
    formData.append("password", password.value);

    const xhr = new XMLHttpRequest();
    xhr.open("POST", "/src/php/login-user.php");
    xhr.send(formData);

    xhr.onload = function() {
        if (this.status === 200) {
            window.location.href = "/";
        } else {
          const response = JSON.parse(this.response);
          alert(response.message);
        }
    }
}
