<?php
include('header.php');
?>
<html>
<head>
  <meta name="csrf_token" content="<?php echo $_SESSION['csrf_token'] ?>">
  <script src="/src/public/cart_controller.js"></script>
  <link rel="stylesheet" type="text/css" href="/src/public/cart.css">
  <script>
    window.onload = init
  </script>
</head>
<body>
  <h1 class='title'>My Cart</h1>
  <div id="container"></div>
</body>
</html>
