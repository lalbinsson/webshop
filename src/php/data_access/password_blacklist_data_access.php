<?php
function is_password_blacklisted($responseHandler, $password) {
  $DEBUG = false;

  $query = 'SELECT COUNT(1) FROM password_blacklist WHERE password = ?';
  $mysqli = create_mysqli();

  $stmt = $mysqli->prepare($query);
  if ($stmt &&
			$stmt->bind_param('s', $password) &&
			$stmt->execute() &&
			$stmt->bind_result($count) &&
			$stmt->store_result() &&
			$stmt->fetch()
	) {
    return $count > 0;
  } else {
    if ($DEBUG) {
			echo 'Error fetching password blacklist: ' . $mysqli->error;
		}
		if ($responseHandler != null) {
      $responseHandler->setAsInternalServerError();
    }
		$mysqli->close();
		return true;
  }
}
?>
