<?php
$DEBUG = false;
function fetch_all_products($responseHandler) {
	$query = 'SELECT * from products WHERE show_in_shop = 1';
	$mysqli = create_mysqli();

	if ($result = $mysqli->query($query)) {
		$products = create_products_array_from_sql_result($result);
		if ($responseHandler != NULL) {
			$responseHandler->setValidRequest(HttpResponseHandler::$status_ok, "Fetch successful.");
		}
	} else {
		if ($DEBUG) {
			echo "ERROR: Was not able to execute $query";
		}

		if ($responseHandler != NULL) {
			$responseHandler->setAsInternalServerError();
		}
	}

	$mysqli->close();

	return $products;
}

function fetch_cart_products($cart, $responseHandler) {
	$product_ids = array_keys($cart);

	$query = 'SELECT * FROM products';
	$conditions = [];
	$param_types = '';

	for ($i = 0; $i < count($product_ids); $i++) {
		$conditions[] = 'product_id = ?';
		$param_types .= 's';
	}

	$params = array();
	$params[] = & $param_types;

	for ($i = 0; $i < count($product_ids); $i++) {
		$params[] = & $product_ids[$i];
	}

	if ($conditions) {
		$query .= ' WHERE '.implode(' OR ', $conditions);

		$mysqli = create_mysqli();
		$stmt = $mysqli->prepare($query);
		if ($stmt &&
				call_user_func_array(array($stmt, 'bind_param'), $params) &&
				$stmt->execute() &&
				$result = $stmt->get_result()
		) {
			$products = create_products_array_from_sql_result($result);
			return $products;
		}
	} else {
		// empty
	}
}

function fetch_product_by_id($product_id, $responseHandler) {
	$query = 'SELECT * from products WHERE show_in_shop = 1 AND product_id = ?';
	$mysqli = create_mysqli();

	$stmt = $mysqli->prepare($query);
	if ($stmt &&
			$stmt->bind_param('s', $product_id) &&
			$stmt->execute() &&
			$result = $stmt->get_result()
	) {
		$products = create_products_array_from_sql_result($result);
		$responseHandler->setValidRequest(HttpResponseHandler::$status_ok, "Fetch successful.");
	} else {
		if ($DEBUG) {
			echo "ERROR: Was not able to execute $query";
		}
		$responseHandler->setAsInternalServerError();
	}

	$mysqli->close();

	return $products;
}

function fetch_product_by_title($product_title, $responseHandler) {
	$query = 'SELECT * from products WHERE show_in_shop = 1 AND title = ?';
	$mysqli = create_mysqli();

	$stmt = $mysqli->prepare($query);
	if ($stmt &&
			$stmt->bind_param('s', $product_title) &&
			$stmt->execute() &&
			$result = $stmt->get_result()
	) {
		$products = create_products_array_from_sql_result($result);
		if ($responseHandler != NULL) {
			$responseHandler->setValidRequest(HttpResponseHandler::$status_ok, "Fetch successful.");
		}
	} else {
		if ($DEBUG) {
			echo "ERROR: Was not able to execute $query";
		}
		if ($responseHandler != NULL) {
			$responseHandler->setAsInternalServerError();
		}
	}

	$mysqli->close();

	return $products;
}

function create_products_array_from_sql_result($sql_result) {
	$products = array();

	while ($row = $sql_result->fetch_array()) {
		$products[] = array(
			'productId' => $row['product_id'],
			'title' => $row['title'],
			'description' => $row['description'],
			'price' => $row['price'],
			'quantityInStock' => $row['quantity_in_stock']
		);
	}

	return $products;
}
?>
