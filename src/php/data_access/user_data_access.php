<?php
function fetch_users_login_attempts($responseHandler, $email) {
	$DEBUG = false;
	$query = 'SELECT login_attempts FROM users WHERE email = ?';
	$mysqli = create_mysqli();

	$stmt = $mysqli->prepare($query);

	if ($stmt &&
			$stmt->bind_param('s', $email) &&
			$stmt->execute() &&
			$stmt->bind_result($login_attempts) &&
			$stmt->store_result() &&
			$stmt->fetch()
	) {
		$mysqli->close();
		return $login_attempts;
	} else {
		if ($DEBUG) {
			echo 'Error fetching users login attempts: ' . $mysqli->error;
		}
		$responseHandler->setAsInternalServerError();
		$mysqli->close();
		return false;
	}
}

function update_users_login_attempts($responseHandler, $email, $login_attempts) {
	$DEBUG = false;
	$statement = 'UPDATE users SET login_attempts = ? WHERE email = ?';
	$mysqli = create_mysqli();
	$stmt = $mysqli->prepare($statement);

	if ($stmt &&
			$stmt->bind_param('is', $login_attempts, $email) &&
			$stmt->execute()
	) {
		// ok
	} else {
		if ($DEBUG) {
			echo 'Error inserting into users login attempts: ' . $mysqli->error;
		}
		$responseHandler->setAsInternalServerError();
	}

	$mysqli->close();
}

function add_lock_timestamp($responseHandler, $email) {
	$DEBUG = false;
	$statement = 'UPDATE users SET lock_timestamp = ? WHERE email = ?';

	$mysqli = create_mysqli();
	$stmt = $mysqli->prepare($statement);

	if ($stmt &&
			$stmt->bind_param('ss', time(), $email) &&
			$stmt->execute()
	) {
		// ok
	} else {
		if ($DEBUG) {
			echo 'Error inserting into users login attempts: ' . $mysqli->error;
		}
		$responseHandler->setAsInternalServerError();
	}

	$mysqli->close();
}

function fetch_users_lock_timestamp($responseHandler, $email) {
	$DEBUG = false;
	$query = 'SELECT lock_timestamp FROM users WHERE email = ?';
	$mysqli = create_mysqli();

	$stmt = $mysqli->prepare($query);

	if ($stmt &&
			$stmt->bind_param('s', $email) &&
			$stmt->execute() &&
			$stmt->bind_result($timestamp) &&
			$stmt->store_result() &&
			$stmt->fetch()
	) {
		$mysqli->close();
		return $timestamp;
	} else {
		if ($DEBUG) {
			echo 'Error fetching users password hash: ' . $mysqli->error;
		}
		$responseHandler->setAsInternalServerError();
		$mysqli->close();
		return false;
	}
}

function fetch_users_password($responseHandler, $email) {
	$DEBUG = false;
	$query = 'SELECT password_hash FROM users WHERE email = ?';
	$mysqli = create_mysqli();

	$stmt = $mysqli->prepare($query);

	if ($stmt &&
			$stmt->bind_param('s', $email) &&
			$stmt->execute() &&
			$stmt->bind_result($password_hash) &&
			$stmt->store_result() &&
			$stmt->fetch()
	) {
		$mysqli->close();
		return $password_hash;
	} else {
		if ($DEBUG) {
			echo 'Error fetching users password hash: ' . $mysqli->error;
		}
		$responseHandler->setAsInternalServerError();
		$mysqli->close();
		return false;
	}
}

?>
