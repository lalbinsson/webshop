<?php
class HttpResponseHandler {
	public static $status_ok = 200;
	public static $status_created = 201;
	public static $status_bad_request = 400;
	public static $status_unauthorized = 401;
	public static $status_forbidden = 403;
	public static $status_not_found = 404;
	public static $status_conflict = 409;
	public static $status_internal_server_error = 500;

	private $status_code;
	private $message;
	private $isRequestValid;

	function __construct($isRequestValid) {
		$this->isRequestValid = $isRequestValid;
	}

	function setStatusCode($status_code) {
		$this->status_code = $status_code;
	}

	function setAsInternalServerError() {
		$this->isRequestValid = false;
		$this->status_code = self::$status_internal_server_error;
		$this->message = "Something went wrong. Please try again.";
	}

	function setInvalidRequest($status_code, $message) {
		$this->isRequestValid = false;
		$this->status_code = $status_code;
		$this->message = $message;
	}

	function setValidRequest($status_code, $message) {
		$this->isRequestValid = true;
		$this->status_code = $status_code;
		$this->message = $message;
	}

	function setMessage($message) {
		$this->message = $message;
	}

	function setRequestAsValid() {
		$this->isRequestValid = true;
	}

	function setRequestAsInvalid() {
		$this->isRequestValid = false;
	}

	function statusCode() {
		return $this->status_code;
	}

	function message() {
		return $this->message;
	}

	function isRequestValid() {
		return $this->isRequestValid;
	}
}

?>
