<?php
	$DEBUG = false;
	require "utils/HttpResponseHandler.php";
	include ('../../connection.php');
	include ('data_access/user_data_access.php');
	include ('on_successful_login.php');
	session_start();

	$LOGIN_ATTEMPTS_LIMIT = 3;
	$LOCKOUT_TIME_MINUTES = 10;

	$responseHandler = new HttpResponseHandler(true);

	if ($_SERVER['REQUEST_METHOD'] === 'GET') {
		$responseHandler->setValidRequest(HttpResponseHandler::$status_ok, '');
		http_response_code($responseHandler->statusCode());
		echo json_encode([
			'successful' => $responseHandler->isRequestValid(),
			'message' => $responseHandler->message(),
			'loggedInUsersName' => $_SESSION['username']
		]);
	} else {
		$email = $_POST['email'];
		$login_attempts = fetch_users_login_attempts($responseHandler, $email);
		if ($login_attempts < $LOGIN_ATTEMPTS_LIMIT) {
			$is_locked = false;
		} else {
			$lock_timetamp = fetch_users_lock_timestamp($responseHandler, $email);
			$should_unlock_account = (time() - $lock_timetamp) / 60 > $LOCKOUT_TIME_MINUTES;

			if ($should_unlock_account) {
				$is_locked = false;
			} else {
				$is_locked = true;
			}
		}

		if (!$is_locked) {
			$password_input = $_POST['password'];
			$correct_password_hash = fetch_users_password($responseHandler, $email);

			if ($responseHandler->isRequestValid()) {
				if (password_verify($password_input, $correct_password_hash)) {
					$responseHandler->setValidRequest(HttpResponseHandler::$status_ok, 'Login successful.');
					update_users_login_attempts($responseHandler, $email, 0);
					onSuccessfulLogin($email);
				} else {
					$responseHandler->setInvalidRequest(HttpResponseHandler::$status_unauthorized, 'Wrong email or password. Please try again.');
					if ($should_unlock_account) {
						$updated_login_attempts = 1;
					} else {
						$updated_login_attempts = $login_attempts + 1;
					}
					update_users_login_attempts($responseHandler, $email, $updated_login_attempts);

					if ($updated_login_attempts == $LOGIN_ATTEMPTS_LIMIT) {
						 add_lock_timestamp($responseHandler, $email);
					}
				}
			}
		} else {
			$responseHandler->setInvalidRequest(HttpResponseHandler::$status_forbidden, 'Your account has beend locked. Please check your email for instruction on how to reset your account.');
		}

		http_response_code($responseHandler->statusCode());
		echo json_encode([
			'successful' => $responseHandler->isRequestValid(),
			'message' => $responseHandler->message()
		]);
	}
?>
