<?php
  session_start()
?>

<!DOCTYPE html>
<html>
<head>
    <link
			href="/src/public/header.css"
			rel="stylesheet"
			media="screen"
		>
		<script src="/src/public/logout.js"></script>
		<script src="/src/public/login.js"></script>
</head>
<body>

  <!-- navbar -->
  <div class="navbar navbar-default navbar-static-top" role="navigation">
      <div class="container">
          <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                  <li>
                      <a href="/">Shop</a>
                  </li>
                  <li>
                      <a href="/src/php/cart.php">
                          Cart <span class="badge" id="cartCount"><?php
                                              if (isset($_SESSION['cartCount'])) {
                                                echo $_SESSION['cartCount'];
                                              } else {
                                                echo "0";
                                              }?></span>
                      </a>
                  </li>
                  <?php
                    if ($_SESSION['logged_in']) {
                      echo "<li onclick='logout()'>";
                      echo "<a href>Logout</a>";
                      echo "</li>";

                      echo "<li id='displayUser'>";
                      echo "<img src='/src/img/user.png'>";
                      echo "<p class='displayUserText'>" .htmlentities($_SESSION['username']) ."</p>";
                      echo "</li>";
                    } else {
                      echo "<li>";
                      echo "<a href='/src/php/login.php'>Login</a>";
                      echo "</li>";
                    }
                  ?>
              </ul>

          </div>
      </div>
  </div>
