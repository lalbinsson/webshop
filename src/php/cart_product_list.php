<?php
  session_start();
  include ('data_access/products_data_access.php');
  include ('../../connection.php');
?>
<ul class='cart'>
  <?php
    $total = 0;
    $cart_empty = false;
    if (isset($_SESSION['cart'])) {
      if (count($_SESSION['cart']) === 0) {
        $cart_empty = true;
      } else {
        $products = fetch_cart_products($_SESSION['cart'], null);
        for ($i = 0; $i < count($products); $i++) {
          $product = $products[$i];
          $productId = $product['productId'];
          $quantity = $_SESSION['cart'][$productId];
          echo "<div class='cartItem' id='productId' value=" . $productId . ">";
          echo  "<img src='/src/img/" . $product['productId'] . ".jpg' style='width:80px'>";
          echo "<p class='prodTitle'>" . $product['title'] . "</p>";
          echo "<p class='description'>" . $product['description'] . "</p>";
          echo "<div class='product-quantity'>";
          echo "<input type='text' id=" .$productId  ." onchange='removeFromCart(this.id, false, this.value)' min=\"1\" value=" .$quantity .">";
          echo "</div>";
          echo "<p class='price'>" . $product['price'] . " SEK</p>";
          echo "<p><button id=" .$productId ." onclick='removeFromCart(this.id, true)'>Remove</button></p>";
          echo "</div>";
          $total= $total + $product['price'] * $quantity;
        }
      }
    } else {
      $cart_empty = true;
    }

    if ($cart_empty) {
      echo "<div class='prodTitle'>Your cart is empty</div>";
    } else if ($_SESSION['logged_in']) {
      ?>
      <div class="total">
        <div class="total-price">Total: <?php echo $total;  ?> SEK</div>
        <a href="/src/php/checkout.php">
          <button class="checkoutBtn">Checkout</button>
        </a>
      </div>
    <?php
  } else {
  ?>
    <div class="total">
      <div class="total-price">Total: <?php echo $total;  ?> SEK</div>
      <a href="/src/php/login.php">
        <button class="checkoutBtn">Log in before checkout</button>
      </a>
    </div>
  <?php
  }
  ?>
</ul>
