<?php
session_start();
include('header.php');

if ($_SESSION['logged_in']) {
?>
  <html>
  <head>
    <script src="/src/public/cart_controller.js"></script>
    <link rel="stylesheet" type="text/css" href="/src/public/cart_receipt.css">
    <script>
      window.onload = checkoutCart
    </script>
  </head>
  <body>
    <h1 class='title'>Your Receipt</h1>
    <div id="container"></div>
  </body>
  </html>
<?php
} else {
  readfile('../public/unauthorized.html');
}
?>
