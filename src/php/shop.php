<?php
$DEBUG = false;
include ('header.php');
include ('connection.php');
include ('data_access/products_data_access.php');

$conn = create_mysqli();
?>
<html>
<head>
  <meta name="csrf_token" content="<?php echo $_SESSION['csrf_token'] ?>">
  <script src="/src/public/cart_controller.js"></script>
  <script>
    function search() {
      const search = document.getElementById('search').value
      window.location.href = "/?q=" + search;
    }
  </script>
  <link rel="stylesheet" type="text/css" href="/src/public/shop.css">
        </head>
        <input id="search" type = "text" class = "data"/>
        <button type="button" onclick="search()">Search</button>
        <?php
          if (isset($_GET['q'])) {
            $search = $_GET['q'];
            if ($search === '') {
              $search = 'all';
            } else {
              echo "<br><label>Results for: </label>";
              echo htmlentities($search, ENT_QUOTES);
            }
          } else {
            $search = 'all';
          }
        ?>
        <body><ul class='products'>
          <?php
                        if ($search === 'all') {
                          $products = fetch_all_products(null);
                        } else {
                          $products = fetch_product_by_title($search, $responseHandler);
                        }


                        if (count($products) === 0) {
                          echo "<label id='no-results-label'>No results found</label>";
                        } else {
                          for ($i = 0; $i < count($products); $i++) {
                              $product = $products[$i];
                              echo "<div class='card'>";
                              echo "<div id='product_id' type='hidden' value=" . $product['productId'] . "></div>";
                              echo  "<img src='/src/img/" . $product['productId'] . ".jpg' style='width:100px'>";
                              echo "<p id='title' value=" . $product['title'] . " >" . $product['title'] . "</p>";
                              echo "<p class='price' id='title' value=" . $product['price'] . " >" . $product['price'] . " SEK</p>";
                              echo "<p class='description' id='description' value=" . $product['description'] . " '>" . $product['description'] . "</p>";
                              echo "<button id=" .$product['productId'] ." onclick='addToCart(this.id)'>Add to Cart</button>";
                              echo "</div>";
                          }  
                        }
          ?>
</ul></body>
</html>
