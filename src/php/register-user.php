<?php
	$DEBUG = false;
	session_start();
	include ('../../connection.php');
	include ('../sql_generators/users_sql.php');
	require "utils/HttpResponseHandler.php";
	include ('on_successful_login.php');
	include ('PasswordCheck.php');

	$responseHandler = new HttpResponseHandler(true);
	$passwordCheck = new PasswordCheck();

	if ($_POST['q'] === 'checkPasswordFormat') {
		if ($passwordCheck->check_password($_POST['password'])) {
			echo '';
		} else {
			echo $passwordCheck->get_message();
		}
	} else if ($_POST['q'] === 'registerUser') {
		if ($passwordCheck->check_password($_POST['password'])) {
			registerUser($responseHandler);
		} else {
			$responseHandler->setInvalidRequest(HttpResponseHandler::$status_bad_request, $passwordCheck->get_message());
		}

		http_response_code($responseHandler->statusCode());
		echo json_encode([
			'successful' => $responseHandler->isRequestValid(),
			'message' => $responseHandler->message(),
		]);
	}



	function registerUser($responseHandler) {
		$DEBUG = false;
		$first_name = $_POST['firstName'];
		$last_name = $_POST['lastName'];
		$email = $_POST['email'];
		$street = $_POST['street'];
		$postal_code = $_POST['postalCode'];
		$city = $_POST['city'];
		$password = $_POST['password'];
		$confirm_password = $_POST['confirmPassword'];

		if ($password !== $confirm_password) {
			$responseHandler->setInvalidRequest(HttpResponseHandler::$status_bad_request, "The passwords did not match. Please try again.");
		} else {
			$mysqli = create_mysqli();

			// Check if email is already registered
			$stmt = $mysqli->prepare(prepStmtCountUsersWithEmail());
			if ($stmt &&
					$stmt->bind_param('s', $_POST['email']) &&
					$stmt->execute() &&
					$stmt->bind_result($count) &&
					$stmt->store_result() &&
					$stmt->fetch()
			) {
				if ($count > 0) {
					// Email is already registered
					$responseHandler->setInvalidRequest(HttpResponseHandler::$status_conflict, "The email is already registrered.");
				}
			} else {
				if ($DEBUG) {
					http_response_code($responseHandler->statusCode());
					echo 'Error checking email: ' . $mysqli->error;
				}
				$responseHandler->setAsInternalServerError();
			}

			if ($responseHandler->isRequestValid()) {
				//
				$stmt = $mysqli->prepare(prepStmtRegisterUser());

				$id = '238JDS' + microtime();
				$password_hash = password_hash($_POST['password'], PASSWORD_DEFAULT);

				if ($stmt &&
						$stmt->bind_param(userSqlTypeString(), $id, $first_name, $last_name, $email, $street, $postal_code, $city, $password_hash) &&
						$stmt->execute()
				) {
					$responseHandler->setValidRequest(HttpResponseHandler::$status_created, "Registration successful.");
					onSuccessfulLogin($email);
				} else {
					$responseHandler->setAsInternalServerError();
					if ($DEBUG) {
						http_response_code($responseHandler->statusCode());
						echo ' Error registering new user: ' . $mysqli->error;
					}
				}
			}

			$mysqli->close();
		}
	}
?>
