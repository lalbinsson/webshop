<?php
	$DEBUG = false;
	require 'utils/HttpResponseHandler.php';

	session_start();

	$responseHandler = new HttpResponseHandler(true);

	if ($_SESSION['logged_in']) {
		session_destroy();
		$responseHandler->setValidRequest(HttpResponseHandler::$status_ok, "Logout successful.");
	} else {
		$responseHandler->setValidRequest(HttpResponseHandler::$status_bad_request, "Not logged in.");
	}

	http_response_code($responseHandler->statusCode());
	echo json_encode([
		'successful' => $responseHandler->isRequestValid(),
		'message' => $responseHandler->message()
	]);
?>
