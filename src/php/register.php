<?php
include ('header.php');
?>
<html>
   <head>
         <script src="../public/register.js"></script>
         <link rel="stylesheet" type="text/css" href="../public/login.css">
      <title>Register</title>
   </head>

            <h1 class="title">Register</h1>
               <form action = "" method = "post">
                  <label>First name:</label><input id="firstName" type = "text" class = "box"/>
                  <label>Last name:</label><input id="lastName" type = "text" class = "box"/>
                  <label>Email:</label><input id="email" type = "text" class = "box"/>
									<label>Street:</label><input id="street" type = "text" class = "box"/>
									<label>Postal code:</label><input id="postalCode" type = "text" class = "box"/>
									<label>City:</label><input id="city" type = "text" class = "box"/>
                  <label>Password:</label>
                  <input id="password" type = "password" class = "box" onkeyup="onPasswordInput(this.value)" />
                  <label id="passwordValidLabel"></label>

                  <label>Confirm password:</label>
                  <input id="confirmPassword" type = "password" class = "box" onkeyup="checkPasswordMatch(this.value)" />
                  <label id="passwordMatchLabel"></label>
                  <button type="button" class="regBtn" onclick="register()">Register</button>
               </form>

            </div>

         </div>

      </div>

   </body>
</html>
