<?php
	$DEBUG = false;
	session_start();
	require "utils/HttpResponseHandler.php";
	$responseHandler = new HttpResponseHandler(true);

	$product_id = $_REQUEST['productId'];
	$csrf_token = $_REQUEST['csrf_token'];

	if (!$_SESSION['logged_in'] || $_SESSION['csrf_token'] === $csrf_token) {
		if ($product_id) {
			if (isset($_SESSION['cart'][$product_id])) {
				$_SESSION['cart'][$product_id] += 1;
			} else {
				$_SESSION['cart'][$product_id] = 1;
			}

			$_SESSION['cartCount'] += 1;

			http_response_code($responseHandler->statusCode());
			echo json_encode([
			  'successful' => $responseHandler->isRequestValid(),
			  'message' => $responseHandler->message(),
				'cartCount' => $_SESSION['cartCount']
			]);
		}
	}
?>
