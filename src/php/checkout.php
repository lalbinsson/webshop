<?php
  session_start();
  include ('header.php');
  include ('../../connection.php');
  $conn = create_mysqli();

  if ($_SESSION['logged_in']) {
?>
   <html>
      <head>
        <meta name="csrf_token" content="<?php echo $_SESSION['csrf_token'] ?>">
          <script src="/src/public/cart_controller.js"></script>
            <link rel="stylesheet" type="text/css" href="../public/checkout.css">
      </head>
      <body><h1 class='title'>Checkout</h1>

         <div class="input-fields">
         <div class="column">
           <label for="cardholder">Cardholder's Name</label>
           <input type="text" id="cardholder" />

           <label for="cardnumber">Card Number</label>
           <input type="text" id="cardnumber"/>

           <label for="date">Valid thru</label>
           <input type="text" id="date" placeholder="MM / YY" />

           <label for="verification">CVV / CVC *</label>
           <input type="text" id="verification"/>
         <div >
           <a href="/src/php/cart_receipt.php"><button class="payBtn" onclick="resetCart()">Confirm payment</button></a>
         </div>
     </div>
   </div>
   </body>
   </html>

 <?php
 } else {
   readfile('../public/unauthorized.html');
 }
?>
