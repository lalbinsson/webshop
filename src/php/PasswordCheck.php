<?php
include ('data_access/password_blacklist_data_access.php');
class PasswordCheck {
  private $message;

  function check_password($password) {
    if ($this->is_password_weak($password)) {
      $this->message = 'Password has to be at least 8 characters long and contain at least one upper case letter, one digit and one special character.';
      return false;
    }

    if (is_password_blacklisted(null, $password)) {
      $this->message = 'Password is too common. Please try something else.';
      return false;
    }

    $this->message = 'Password ok.';
    return true;
  }

  function get_message() {
    return $this->message;
  }

  private function is_password_weak($password) {
    if (strlen($password) < 8) {
      return true;
    }

    if (preg_match('/[^\w]/', $password) === 0) {
      return true;
    }

    if (preg_match('/\p{Lu}/', $password) === 0) {
      return true;
    }

    if (preg_match('/\d/', $password) === 0) {
      return true;
    }

    return false;
  }
}
?>
