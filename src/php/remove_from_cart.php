<?php
	$DEBUG = false;
	session_start();
	require "utils/HttpResponseHandler.php";
	include ('../../connection.php');

	$responseHandler = new HttpResponseHandler(true);

	$product_id = $_REQUEST['productId'];
	$csrf_token = $_REQUEST['csrf_token'];

	if (!$_SESSION['logged_in'] || $_SESSION['csrf_token'] === $csrf_token) {
		if (isset($_SESSION['cart'][$product_id])) {
			$responseHandler->setValidRequest(HttpResponseHandler::$status_ok, "Delete successful.");

			$unset = $_REQUEST['removeAll'] === 'true';
			if (!$unset) {
				$previous_quantity = $_SESSION['cart'][$product_id];
				$_SESSION['cart'][$product_id] = $_REQUEST['newQuantity'];
				$quantity_diff = $previous_quantity - $_REQUEST['newQuantity'];

				$_SESSION['cartCount'] -= $quantity_diff;
			} else {
				$_SESSION['cartCount'] -= $_SESSION['cart'][$product_id];
			}

			if ($unset || $_SESSION['cart'][$product_id] === 0) {
				unset($_SESSION['cart'][$product_id]);
			}
		} else {
			$responseHandler->setValidRequest(HttpResponseHandler::$status_bad_request, "Product does not exist in cart.");
		}

		http_response_code($responseHandler->statusCode());
		echo json_encode([
		  'successful' => $responseHandler->isRequestValid(),
		  'message' => $responseHandler->message(),
			'cartCount' => $_SESSION['cartCount']
		]);
	}

?>
