<?php
  function onSuccessfulLogin($email) {
    if (isset($_SESSION['cart'])) {
      $prev_cart = $_SESSION['cart'];
      $prev_count = $_SESSION['cartCount'];
    } else {
      $prev_cart = [];
      $prev_count = 0;
    }

    session_regenerate_id();
    $_SESSION['username'] = $email;
    $_SESSION['logged_in'] = true;
    $_SESSION['cart'] = $prev_cart;
    $_SESSION['cartCount'] = $prev_count;
    $_SESSION['csrf_token'] = bin2hex(random_bytes(32));
  }
?>
