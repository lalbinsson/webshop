<?php
$DEBUG = false;
require "utils/HttpResponseHandler.php";
include ('data_access/products_data_access.php');

$responseHandler = new HttpResponseHandler(true);

if ($q = $_GET['q']) {
	if ($q === 'all') {
		$products = fetch_all_products($responseHandler);
		echo_result($responseHandler, $products);
	} else {
		$products = fetch_product_by_id($q, $responseHandler);
		echo_result($responseHandler, $products);
	}
} else {
	if ($DEBUG) {
		echo 'No URL parameter q.';
	}

	$responseHandler->setAsInternalServerError();
}

function echo_result($responseHandler, $products) {
	echo json_encode([
		'successful' => $responseHandler->isRequestValid(),
		'message' => $responseHandler->message(),
		'products' => $products
	]);
}

?>
