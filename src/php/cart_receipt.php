<?php
  session_start();
  include('header.php');
  include ('data_access/products_data_access.php');
  include ('../../connection.php');

  if ($_SESSION['logged_in']) {
?>
<script>
  window.onload = resetCart
</script>
  <script src="/src/public/cart_controller.js"></script>
    <link rel="stylesheet" type="text/css" href="/src/public/cart_receipt.css">
    <h1 class='title'>Your Receipt</h1>
    <div id="container"></div>
  <ul class='cart'>
  <?php
    $products = fetch_cart_products($_SESSION['cart'], null);
    for ($i = 0; $i < count($products); $i++) {
      $product = $products[$i];
      $productId = $product['productId'];
      $quantity = $_SESSION['cart'][$productId];
      echo "<div class='cartItem' id='productId' value=" . $productId . ">";
      echo "<p class='prodTitle'>" . $product['title'] . "</h1>";
      echo "<p class='product-quantity'> " .$quantity . " st";
      echo "</p>";
      echo "<p class='price'> á " . $product['price'] . " SEK</p>";
      echo "</div>";
      $total= $total + $product['price'] * $quantity;
    }
  ?>

  <div class="total-price">Total: <?php echo $total;  ?> SEK</div>
</ul>
<?php
  unset($_SESSION['cart']);
  $_SESSION['cartCount'] = 0;
?>
<?php
} else {
  readfile('../public/unauthorized.html');
}
?>
